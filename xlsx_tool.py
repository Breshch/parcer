from openpyxl import load_workbook
import tkinter as tk
from tkinter import filedialog as fd
from test import get_price

def get_path():
    root = tk.Tk()
    root.withdraw()
    return fd.askopenfilename()



def get_articles():
    path = get_path()
    '''df = pd.read_excel(path) # can also index sheet by name or fetch all sheets
    mylist = df['A'].tolist()'''
    wb = load_workbook(path)  # Work Book
    ws = wb.get_sheet_by_name('Лист1')  # Work Sheet
    column = ws['A']  # Column
    column_list = [column[x].value for x in range(len(column))]
    for article in column_list:
        articleList = get_price(article)
        print(articleList[2])


get_articles()